<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
include_once "php/Config.php";
include_once "php/Bookmaker.php";

$return = array(
    'league' => "",
    'curl' => array()
);
$cnf = new Config();


//Prendo tutti i bookamer del campionato con aggiornamento più vecchio (last_update)
$query = "
SELECT L.*, C.*
FROM league AS L
JOIN curl AS C ON L.lid = C.lid
WHERE C.active = 1 AND L.lid = (
                SELECT lid
                FROM league
                ORDER BY last_update ASC
                LIMIT 1)";
$exec_global = mysqli_query($cnf->getDb(), $query);

$i=0;
while ( $curl = mysqli_fetch_assoc($exec_global)) {
    $return['league'] = $curl['lid'];
    $return['curl'][$i]['league'] = $curl;
    $content = "";
    $db_headers = array();
    $db_options = array();

    $query = "SELECT * FROM curl_option AS CO WHERE CO.cid = ".$curl['cid']." AND CO.type='header'";
    $exec = mysqli_query($cnf->getDb(), $query);
    while( $tmp = mysqli_fetch_array($exec)) $db_headers[] = $tmp;

    $query = "SELECT * FROM curl_option AS CO WHERE CO.cid = ".$curl['cid']." AND CO.type!='header'";
    $exec = mysqli_query($cnf->getDb(), $query);
    while( $tmp = mysqli_fetch_array($exec)) $db_options[] = $tmp;

    //Genero la curl
    $ch = Bookmaker::generateCurl($curl, $db_headers, $db_options);

    //curl_setopt($ch, CURLINFO_HEADER_OUT, true);

    //Eseguo
    $content = curl_exec($ch);

    //die (var_dump(curl_getinfo($ch)["request_header"]));
    curl_close($ch);

    $return['curl'][$i++]['content'] = htmlspecialchars($content);
}

echo json_encode($return);
