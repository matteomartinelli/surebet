<?php

?>

<html>
<head>
	<title>SUREBET bot</title>
	<script src="js/jquery-2.1.4.min.js"></script>

	<script src="js/parser/better.js"></script>
	<script src="js/parser/unibet.js"></script>
	<script src="js/parser/sisal.js"></script>
	<script src="js/parser/betclic.js"></script>
	<script src="js/parser/snai.js"></script>
	<script src="js/parser/interwetten.js"></script>
	<script src="js/parser/williamhill.js"></script>
	<script src="js/parser/pinnacle.js"></script>
	<script src="js/parser/tipico.js"></script>
	<script src="js/parser/izi.js"></script>
	<script src="js/parser/bet365.js"></script>
	<script src="js/parser/bwin.js"></script>
	<script src="js/parser/betflag.js"></script>
	<script src="js/parser/marathon.js"></script>
	<script src="js/parser/eurobet.js"></script>
	<script src="js/parser/goldbet.js"></script>
	<script src="js/parser/10bet.js"></script>
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<style>
		#log > div{
			margin-bottom: 15px;
			padding-bottom: 15px;
			border-bottom: 1px solid #ccc;
		}
	</style>
</head>

<body>
<h1 style="text-align:center">Sto processando:</h1>
<div id="log">

</div>
<script src="js/app.js"></script>
</body>
</html>
