function parse_pinnacle(content){
    var matches = [];

    var html = $("<div>").html(content);
    var tds = html.find("#mainbox2").find("table.linesTbl tbody tr.linesAlt1,table.linesTbl tbody tr.linesAlt2");

    var this_match;
    $.each(tds, function(i, par){
        if(i%3 == 0){
            var name1 = $(par).find(".linesTeam").html().trim().toLowerCase();
            var bet1 = parseFloat(parseFloat($(par).find(".linesMLine").html().replace(/&nbsp;/g,"").trim()).toFixed(2));
            this_match = {
                name1: name1,
                name2: '',
                pal: '',
                avv: '',
                bet1: bet1,
                wheretobet1: 8,
                betx: 0,
                wheretobetx: 8,
                bet2: 0,
                wheretobet2: 8
            };

        }else if(i%3==1){
            //second
            var name2 = $(par).find(".linesTeam").html().trim().toLowerCase();
            var bet2 = parseFloat(parseFloat($(par).find(".linesMLine").html().replace(/&nbsp;/g,"").trim()).toFixed(2));
            this_match.name2 = name2;
            this_match.bet2 = bet2
        }else{
            //draw
            this_match.betx = parseFloat(parseFloat($(par).find(".linesMLine").html().replace(/&nbsp;/g,"").trim()).toFixed(2));
            matches = matches.concat(this_match);
        }
    });

    return matches;

}
