function parse_eurobet(content){
    var matches = [];

    var partite = $(content).find(".box_container_scommesse_evento");

    $.each(partite, function(i, par) {
        partita = $(par);
        quote = partita.find(".box_container_scommesse_quote > div");
        nome = partita.find(".box_container_scommesse_nomeEvento a").html().split("-");
        nome1 =  nome[0].trim().toLowerCase();
        nome2 =  nome[1].trim().toLowerCase();

        match = {
            name1: nome1,
            name2: nome2,
            pal: "",
            avv: "",
            bet1: parseFloat($(quote[0]).html()),
            wheretobet1: 15,
            betx: parseFloat($(quote[1]).html()),
            wheretobetx: 15,
            bet2: parseFloat($(quote[2]).html()),
            wheretobet2: 15
        };

        matches = matches.concat(match);
    });
    return matches;

}
