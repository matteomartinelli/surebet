function parse_better(content){
    var matches = [];

    var content=htmlspecialchars_decode(content.substring(content.indexOf('<div'),content.length-5),0);
    content = stripslashes(content.replace(/&quote;/g,'"').replace(/\\n/g,"").replace(/\\r/g,"").replace(/\\t/g,""));
    var html=$(content);
    var div=html.find(".btRowHolder.even");
    $.each(div, function(i, par){
        partita = $(par);
        if(partita.find("div").length > 4) {
            divs = partita.find("div");
            nome =  $(divs[1]).find(".match").text().split("-");
            info = $(divs[1]).find(".match").attr('onclick').replace("javascript:getAvvenimento(","").replace(")","").split(',');
            bet1 = $(divs[4]).find(".betValue").val();
            betx = $(divs[5]).find(".betValue").val();
            bet2 = $(divs[6]).find(".betValue").val();

            match = {
                name1: nome[0].trim().toLowerCase(),
                name2: nome[1].trim().toLowerCase(),
                pal: parseInt(info[0]),
                avv: parseInt(info[1]),
                bet1: parseFloat(bet1),
                wheretobet1: 1,
                betx: parseFloat(betx),
                wheretobetx: 1,
                bet2: parseFloat(bet2),
                wheretobet2: 1
            };

            matches = matches.concat(match);

        }
    });

    return matches;

}
