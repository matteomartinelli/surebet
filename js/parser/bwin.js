function parse_bwin(content){
    var matches = [];
    var html=$(content);
    var table=html.find("table.options");
    $.each(table, function(i, par) {
        partita = $(par);
        if(partita.find("td").length > 2) {
            tds = partita.find("td");
            nome1 =  $(tds[0]).find(".option-name").text();
            nome2 =  $(tds[2]).find(".option-name").text();
            bet1 = $(tds[0]).find(".odds").text();
            betx = $(tds[1]).find(".odds").text();
            bet2 = $(tds[2]).find(".odds").text();

            match = {
                name1: nome1.trim().toLowerCase(),
                name2: nome2.trim().toLowerCase(),
                pal: "",
                avv: "",
                bet1: parseFloat(bet1),
                wheretobet1: 12,
                betx: parseFloat(betx),
                wheretobetx: 12,
                bet2: parseFloat(bet2),
                wheretobet2: 12
            };

            matches = matches.concat(match);
        }
    });
    //console.log(matches);
    return matches;

}
