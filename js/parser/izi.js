function parse_izi(content){
    var matches = [];
    var html=$(htmlspecialchars_decode(content));
    var tr=html.find(".avvenimento tr.dataAvv");
    $.each(tr, function(i, par){
        partita = $(par);
        if(partita.find("td").length > 3) {
            tds = partita.find("td");
            nome =  $(tds[0]).find(".descAvv.hideNomeManif").text().split("-");
            info1 = $(tds[0]).find(".descAvv.hideNomeManif").attr('onclick').replace("scommesseAvvenimentoJsp(","").replace(");","").split(',');
            bet1 = $(tds[1]).text();
            betx = $(tds[2]).text();
            bet2 = $(tds[3]).text();

            match = {
                name1: nome[0].trim().toLowerCase(),
                name2: nome[1].trim().toLowerCase(),
                pal: parseInt(info1[0]),
                avv: parseInt(info1[1]),
                bet1: parseFloat(bet1.replace(",",".")),
                wheretobet1: 10,
                betx: parseFloat(betx.replace(",",".")),
                wheretobetx: 10,
                bet2: parseFloat(bet2.replace(",",".")),
                wheretobet2: 10
            };

            matches = matches.concat(match);
        }
    });
    return matches;

}
