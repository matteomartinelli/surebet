function parse_betclic(content){
    var matches = [];

    var structure = JSON.parse(content);
    var html = $(structure.Data.CompetitionEvents);

    lista_partite = html.find(".match-entry");

    $.each(lista_partite, function(i, partita){
        nome = $(partita).find(".match-name");
        quote = $(partita).find(".match-odds .match-odd");

        if(nome.find('.place').html() != undefined){
            nome_squadre = nome.find('a').html().split("-");
            pal = nome.find('.place').html().split("-");

            match = {
                name1: nome_squadre[0].trim().toLowerCase(),
                name2: nome_squadre[1].trim().toLowerCase(),
                pal: parseInt(pal[0].trim()),
                avv: parseInt(pal[1].trim()),
                bet1: parseFloat($(quote[0]).find("span").html().replace(",",".")),
                wheretobet1: 4,
                betx: parseFloat($(quote[1]).find("span").html().replace(",",".")),
                wheretobetx: 4,
                bet2: parseFloat($(quote[2]).find("span").html().replace(",",".")),
                wheretobet2: 4
            };

            matches = matches.concat(match);
        }
    });

    return matches;

}
