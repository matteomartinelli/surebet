function parse_snai(content){
    var matches = [];


    var html = $(content).find("#CALCIO_17789_3_SERIE_A");
    var tr = html.find("tbody tr");

    $.each(tr, function(i, par){
        partita = $(par);
        if(partita.find("td").length > 3){
            tds = partita.find("td");

            nome =  $(tds[1]).find(".linkblu").html().split("-");
            info1 = $(tds[1]).find(".linkblu").attr('href').replace("javascript:vai_pagescommessa_qc(","").replace(");","").replace(/'/g,"").split(',');
            bet1 = $(tds[2]).find(".boxscommesse").attr('onclick').replace("insscommessa_qc(","").replace(");","").split(',');
            betx = $(tds[3]).find(".boxscommesse").attr('onclick').replace("insscommessa_qc(","").replace(");","").split(',');
            bet2 = $(tds[4]).find(".boxscommesse").attr('onclick').replace("insscommessa_qc(","").replace(");","").split(',');

            match = {
                name1: nome[0].trim().toLowerCase(),
                name2: nome[1].trim().toLowerCase(),
                 pal: parseInt(info1[1]),
                 avv: parseInt(info1[2]),
                 bet1: parseFloat(bet1[5]/100),
                 wheretobet1: 3,
                 betx: parseFloat(betx[5]/100),
                 wheretobetx: 3,
                 bet2: parseFloat(bet2[5]/100),
                 wheretobet2: 3
            };

            matches = matches.concat(match);
        }
    });



    return matches;

}
