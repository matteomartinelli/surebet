function parse_sisal(content){
    var matches = [];
//var html = $(content);
    //console.log(JSON.parse(content).scommessaList);

    lista_partite = JSON.parse(content).scommessaList;

    $.each(lista_partite, function(i, partita){
        nome_squadre = partita.descrizioneAvvenimento.split("-");
        match = {
            name1: nome_squadre[0].trim().toLowerCase(),
            name2: nome_squadre[1].trim().toLowerCase(),
            pal: parseInt(partita.codicePalinsesto),
            avv: parseInt(partita.codiceAvvenimento),
            bet1: parseFloat(partita.esitoList[0].formattedQuota),
            wheretobet1: 2,
            betx: parseFloat(partita.esitoList[1].formattedQuota),
            wheretobetx: 2,
            bet2: parseFloat(partita.esitoList[2].formattedQuota),
            wheretobet2: 2
        };

        matches = matches.concat(match);
    });

    return matches;

}
