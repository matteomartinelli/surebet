function parse_williamhill(content){
    var matches = [];
    var html = $("<div>").html(content);
    var table = html.find("#tup_mkt_grp_UC_9d8a08d4b13c912153e27659829a27ad").find("table tbody");

    $.each(table.find("tr"), function(i, partita) {
        var this_match = $(partita);
        var tds = this_match.find("td");

        var nome = $(tds[2]).find("a span").html().replace(/&nbsp;/g,"");
        var encodedStr = nome.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
            return '&#'+i.charCodeAt(0)+';';
        });
        nome = encodedStr.split("&#8331;");

        match = {
            name1: nome[0].trim().toLowerCase(),
            name2: nome[1].trim().toLowerCase(),
            pal: '',
            avv: '',
            bet1: parseFloat($(tds[4]).find("div div").html()),
            wheretobet1: 7,
            betx: parseFloat($(tds[5]).find("div div").html()),
            wheretobetx: 7,
            bet2: parseFloat($(tds[6]).find("div div").html()),
            wheretobet2: 7
        };

        matches = matches.concat(match);
    });

    return matches;

}
