function parse_unibet(content){
    var matches = [];

    lista_partite = JSON.parse(content);

    $.each(lista_partite.events, function(i, partita){
        var quote;
        for(var k=0; k<lista_partite.betoffers.length;k++){
            if(lista_partite.betoffers[k].eventId == partita.id){
                quote = lista_partite.betoffers[k];
                break;
            }
        }

        match = {
            name1: partita.homeName.trim().toLowerCase(),
            name2: partita.awayName.trim().toLowerCase(),
            pal: '',
            avv: '',
            bet1: parseFloat(quote.outcomes[0].odds)/1000,
            wheretobet1: 5,
            betx: parseFloat(quote.outcomes[1].odds)/1000,
            wheretobetx: 5,
            bet2: parseFloat(quote.outcomes[2].odds)/1000,
            wheretobet2: 5
        };

        matches = matches.concat(match);
    });
    return matches;

}
