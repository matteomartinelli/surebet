function parse_interwetten(content){
    var matches = [];

    //console.log(content);
    var html = $("<div>").html(content);
    var table = html.find("#divRoot").find(".colmiddle").find(".bets.shadow").find("table");
    var tr = table.find("tr");

    $.each(tr, function(i, par){
        if($(par).find(".bets").html() != undefined){
            var bets = $(par).find(".bets").find("table").find("tbody").find("tr").find("td");
            var match1 = $(bets[0]).find("p");
            var matchx = $(bets[1]).find("p");
            var match2 = $(bets[2]).find("p");

            match = {
                name1: match1.find("span").html().trim().toLowerCase(),
                name2: match2.find("span").html().trim().toLowerCase(),
                pal: '',
                avv: '',
                bet1: parseFloat(match1.find("strong").html().replace(",",".")),
                wheretobet1: 6,
                betx: parseFloat(matchx.find("strong").html().replace(",",".")),
                wheretobetx: 6,
                bet2: parseFloat(match2.find("strong").html().replace(",",".")),
                wheretobet2: 6
            };

            matches = matches.concat(match);
        }
    });

    return matches;

}
