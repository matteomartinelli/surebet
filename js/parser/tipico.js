function parse_tipico(content){
    var matches = [];

    var html = $("<div>").html(content);
    var partite = html.find(".e_active.t_row.jq-event-row-cont");
     $.each(partite, function(i, par){
         var nomi = $(par).find(".t_cell");
         var bet = $(par).find(".bl.br.left.cf").find(".multi_row").find("div");

         match = {
             name1: $(nomi[0]).html().trim().toLowerCase(),
             name2: $(nomi[1]).html().trim().toLowerCase(),
             pal: '',
             avv: '',
             bet1: parseFloat($(bet[0]).html().replace(",",".")),
             wheretobet1: 9,
             betx: parseFloat($(bet[1]).html().replace(",",".")),
             wheretobetx: 9,
             bet2: parseFloat($(bet[2]).html().replace(",",".")),
             wheretobet2: 9
         };
         matches = matches.concat(match);
    });

    return matches;

}
