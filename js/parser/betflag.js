function parse_betflag(content){
    var matches = [];
    lista_partite=JSON.parse(content).d;

    $.each(lista_partite, function(i, partita){
        match = {
            name1: partita.DAI.toLowerCase(),
            name2: partita.DAO.toLowerCase(),
            pal: parseInt(partita.P),
            avv: parseInt(partita.IA),
            bet1: parseFloat(partita.Quotazioni[0].Q.replace(',','.')),
            wheretobet1: 13,
            betx: parseFloat(partita.Quotazioni[1].Q.replace(',','.')),
            wheretobetx: 13,
            bet2: parseFloat(partita.Quotazioni[2].Q.replace(',','.')),
            wheretobet2: 13
        };
        matches = matches.concat(match);
    });
    return matches;

}
