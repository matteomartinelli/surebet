var wip = false;

function retrieve(){
   //debug("prova retrieve");
   if(wip == false){
      //debug("inizio retrieve");
      wip = true;
      $.ajax({
         url: 'retrieve.php',
         timeout: 0,
         dataType: 'json',
         success: function(response){
            var arr_matches = parse(response.curl);
            save(response.league, arr_matches);
         },
         error: function(){
            location.reload();
         }
      });
   }
}

function save(league, list_matches){
   $.ajax({
      url: 'save.php',
      data:{
         matches: JSON.stringify(list_matches),
         league: league
      } ,
      timeout: 0,
      type: 'POST',
      dataType: 'json',
      success: function(response){
         //debug("fine save");
      },
      error: function(requestObject, error, errorThrown){
         //alert(error);
         //alert(errorThrown);
         //location.reload();
      },
      complete: function(){
         //debug("free retrieve");
         wip = false;
      }
   });
}

function parse(response){
   //debug("inizio parse");

   var matches = [];

   $.each(response, function(i, bookmaker) {
      /*
      * bookmaker.content è dove cè quello da parsare.
      * choose_parse mi sceglie il parse e mi ritorna un array con i match parsati
      * */
       matches = matches.concat(choose_parse(bookmaker.league, htmlspecialchars_decode(bookmaker.content)));
       $("#log").prepend($("<div>").html("<b>time:</b> " + Date() + " <br> <b>league:</b> " + bookmaker.league.lid + "<br><b>bookmaker:</b> " + bookmaker.league.url));
   });

   //debug("finito parse");

   /*
   * valuto tutti i match e scelgo solo quelli da salvare
   * */
   return evaluate(matches);
}

function choose_parse(bookmaker, content){
   var tmp_matches = [];

   switch(bookmaker.bid){
      case "1":
         tmp_matches = parse_better(content);
         break;
      case "2":
         tmp_matches = parse_sisal(content);
         break;
      case "3":
         tmp_matches = parse_snai(content);
         break;
      case "4":
         tmp_matches = parse_betclic(content);
         break;
      case "5":
         tmp_matches = parse_unibet(content);
         break;
      case "6":
         tmp_matches = parse_interwetten(content);
         break;
      case "7":
         tmp_matches = parse_williamhill(content);
         break;
      case "8":
         tmp_matches = parse_pinnacle(content);
         break;
      case "9":
         tmp_matches = parse_tipico(content);
         break;
      case "10":
         tmp_matches = parse_izi(content);
         break;
      case "11":
         tmp_matches = parse_bet365(content);
         break;
      case "12":
         tmp_matches = parse_bwin(content);
         break;
      case "13":
         tmp_matches = parse_betflag(content);
         break;
      case "14":
         tmp_matches = parse_marathon(content);
         break;
      case "15":
         tmp_matches = parse_eurobet(content);
         break;
      case "16":
         tmp_matches = parse_goldbet(content);
         break;
      case "17":
         tmp_matches = parse_10bet(content);
         break;
   }
   return tmp_matches;
}

function evaluate(matches){ //valuta i match con resa > 0
   var conresa = [];
   var count = 0;
   for(var i = 0; i<matches.length; i++){
      if(matches[i] != false){
         this_match = {
            name1: matches[i].name1,
            name2: matches[i].name2,
            pal: matches[i].pal,
            avv: matches[i].avv,
            bet1: matches[i].bet1,
            wheretobet1: matches[i].wheretobet1,
            betx: matches[i].betx,
            wheretobetx: matches[i].wheretobetx,
            bet2: matches[i].bet2,
            wheretobet2: matches[i].wheretobet2
         };
         for (var j = i+1; j<matches.length; j++){
            //debug(matches[i]);
            //debug(matches[j]);
            if(matches[j] != false){
               same_avv = (matches[i].avv != "" && matches[j].avv != "" && matches[i].avv == matches[j].avv);
               same_name = compareName( deleteAccents(matches[i].name1.toLowerCase()), deleteAccents(matches[i].name2.toLowerCase()), deleteAccents(matches[j].name1.toLowerCase()), deleteAccents(matches[j].name2.toLowerCase()));
               //debug("per la partita: "+matches[i].name1+"-"+matches[i].name2+" ||| Il valore same_avv è: "+same_avv+" ||| Il valore di same_name è:"+ same_name);
               //if( (matches[i].avv != "" && matches[j].avv != "" && same_avv) || same_name ){
               if(same_name ){
                  if(this_match.bet1 < matches[j].bet1){
                     this_match.bet1 = matches[j].bet1;
                     this_match.wheretobet1 = matches[j].wheretobet1;
                  }
                  if(this_match.bet2 < matches[j].bet2){
                     this_match.bet2 = matches[j].bet2;
                     this_match.wheretobet2 = matches[j].wheretobet2;
                  }
                  if(this_match.betx < matches[j].betx){
                     this_match.betx = matches[j].betx;
                     this_match.wheretobetx = matches[j].wheretobetx;
                  }

                  matches[j] = false;
               }
            }
         }

         debug(this_match);
         //checko la resa
         debug((1/this_match.bet1 + 1/this_match.bet2 +1/this_match.betx)*100);
         if((1/this_match.bet1 + 1/this_match.bet2 +1/this_match.betx)*100 < 100){
            conresa[count++] = this_match;
         }
         matches[i] = false;
         i=0;
      }
   }

   return conresa;
}

function debug(string){
   console.log(string);
}

$(document).ready(function(){
   setInterval(retrieve, 15000);
   retrieve();
});


function htmlspecialchars_decode(string, quote_style) {
   //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
   //      original by: Mirek Slugen
   //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   //      bugfixed by: Mateusz "loonquawl" Zalega
   //      bugfixed by: Onno Marsman
   //      bugfixed by: Brett Zamir (http://brett-zamir.me)
   //      bugfixed by: Brett Zamir (http://brett-zamir.me)
   //         input by: ReverseSyntax
   //         input by: Slawomir Kaniecki
   //         input by: Scott Cariss
   //         input by: Francois
   //         input by: Ratheous
   //         input by: Mailfaker (http://www.weedem.fr/)
   //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   // reimplemented by: Brett Zamir (http://brett-zamir.me)
   //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
   //        returns 1: '<p>this -> &quot;</p>'
   //        example 2: htmlspecialchars_decode("&amp;quot;");
   //        returns 2: '&quot;'

   var optTemp = 0,
       i = 0,
       noquotes = false;
   if (typeof quote_style === 'undefined') {
      quote_style = 2;
   }
   string = string.toString()
       .replace(/&lt;/g, '<')
       .replace(/&gt;/g, '>');
   var OPTS = {
      'ENT_NOQUOTES': 0,
      'ENT_HTML_QUOTE_SINGLE': 1,
      'ENT_HTML_QUOTE_DOUBLE': 2,
      'ENT_COMPAT': 2,
      'ENT_QUOTES': 3,
      'ENT_IGNORE': 4
   };
   if (quote_style === 0) {
      noquotes = true;
   }
   if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
      quote_style = [].concat(quote_style);
      for (i = 0; i < quote_style.length; i++) {
         // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
         if (OPTS[quote_style[i]] === 0) {
            noquotes = true;
         } else if (OPTS[quote_style[i]]) {
            optTemp = optTemp | OPTS[quote_style[i]];
         }
      }
      quote_style = optTemp;
   }
   if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
      string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
      // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
   }
   if (!noquotes) {
      string = string.replace(/&quot;/g, '"');
   }
   // Put this in last place to avoid escape being double-decoded
   string = string.replace(/&amp;/g, '&');

   return string;
}

function stripslashes(str) {
   //       discuss at: http://phpjs.org/functions/stripslashes/
   //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   //      improved by: Ates Goral (http://magnetiq.com)
   //      improved by: marrtins
   //      improved by: rezna
   //         fixed by: Mick@el
   //      bugfixed by: Onno Marsman
   //      bugfixed by: Brett Zamir (http://brett-zamir.me)
   //         input by: Rick Waldron
   //         input by: Brant Messenger (http://www.brantmessenger.com/)
   // reimplemented by: Brett Zamir (http://brett-zamir.me)
   //        example 1: stripslashes('Kevin\'s code');
   //        returns 1: "Kevin's code"
   //        example 2: stripslashes('Kevin\\\'s code');
   //        returns 2: "Kevin\'s code"

   return (str + '')
       .replace(/\\(.?)/g, function(s, n1) {
          switch (n1) {
             case '\\':
                return '\\';
             case '0':
                return '\u0000';
             case '':
                return '';
             default:
                return n1;
          }
       });
}

function deleteAccents(team) {
   var mapObj = {
      ù:'u',
      à:'a',
      è:'e',
      é:'e',
      ì:'i',
      ò:'o'
   };
   return team.replace(/ù|à|è|é|ì|ò/g,function(matched){
      return mapObj[matched];
   });
}
/*
 * Input: 4 squadre, 2 per book, di due book differenti
 * Output: true se stessa partita, false se partita non corrisponde
 */
function compareName(book1Squadra1, book1Squadra2, book2Squadra1, book2Squadra2 ){
   team1=false;
   team2=false;

   (book1Squadra1==book2Squadra1)?team1=true:team1=false;
   (book1Squadra2==book2Squadra2)?team2=true:team2=false;

   if(team1 && team2){
      return true
      console.log('entrambi i nomi uguali: b11: ' + book1Squadra1 + ' b21: ' + book2Squadra1);
   }else{
      if(team1 || team2){
         if(!team1 && team2){
            if(findShortest(book1Squadra1,book2Squadra1)>60){
               return true;
            }
         }else{
            if(findShortest(book1Squadra2,book2Squadra2)>60){
               return true;
            }
         }
      }else{
         if((findShortest(book1Squadra1,book2Squadra1)+findShortest(book1Squadra2,book2Squadra2))/2>70){
            return true;
         }else{
            return false;
         }

      }
   }

}

function findShortest(word1,word2){
   w1Lenght='';
   if(word1.length<word2.length){
      wordShort=word1;
      wordLong=word2;
   }else{
      wordShort=word2;
      wordLong=word1;
   }
   w1Lenght=wordShort.length;
   while(wordShort.length>2){
      if(wordLong.indexOf(wordShort)>-1){
         return ((wordShort.length*100)/w1Lenght);
      }else{
         wordShort=wordShort.substring(0,(wordShort.length-1));
      }
   }
   return 0;
}