<?php
namespace Crawler\Bookmaker;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Isibet extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("From=". date("d/m/Y") ."&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    }

    public function parse($url){
        $client = new Client();

        parse_str($url, $url_parsed);

        $crawler = $client->request('POST', 'https://media.isibet.it/Sport/CaricaManifestazione', array(
            'IdDisciplina' => '1',
            'IdManifestazione' => '0',
            'Layout' => '10000',
            'Quando' => '1',
            'Size' => '1351'
        ));
        $p1 = array();
        $crawler->filter('.sp_quote_std > tbody > tr')->each(function ($node) use (&$p1){
            $participants = array();

            $node->filter('td > .sp_quote_std_avv_des')->each(function($node_match) use (&$participants){
                $tmp = explode("-", $node_match->text());
                $participants[] = array(
                    'name' => trim($tmp[0]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => "X",
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => trim($tmp[1]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
            });
            $i=0;
            $node->filter("td > .sp_quote_std_quota_div > .sp_quote_std_quota_eti")->each(function($node_odd) use (&$participants, &$i){

                if($i<3){
                    $participants[$i++]['quota'] = (float)trim(str_replace(",",".", $node_odd->text()));
                }
            });
            $p1[]['team'] = $participants;
        });
        echo 'Isibet parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'isibet';
    }
}
