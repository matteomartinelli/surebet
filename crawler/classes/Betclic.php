<?php
namespace Crawler\Bookmaker;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Betclic extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("From=". date("d/m/Y") . "&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    }

    public function parse($url){
        $client = new Client();

        parse_str($url, $url_parsed);

        $crawler = $client->request('GET', 'https://www.betclic.it/calendario/calcio-s1i1'/*, array(
            'From' => $url_parsed['From'],
            'SortBy' => $url_parsed['SortBy'],
            'Live' => $url_parsed['Live'],
            'MultipleBoost' => $url_parsed['MultipleBoost'],
            'Competitions.Selected' => $url_parsed['Competitions_Selected'],
            'Search' => false
        )*/);

        $p1 = array();
        $crawler->filter('#cal-wrapper-prelive > .cal-day-entry > .cal-schedule > .event > .match-entry')->each(function ($node) use (&$p1){
            $participants = array();
            $node->filter('.match-name > a')->each(function($node_match) use (&$participants){
                $tmp = explode("-", $node_match->text());
                $participants[] = array(
                    'name' => trim($tmp[0]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => "X",
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => trim($tmp[1]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
            });

            $i=0;
            $node->filter(".match-odds > .match-odd")->each(function($node_odd) use (&$participants, &$i){
                $participants[$i++]['quota'] = (float)trim(str_replace(",",".", $node_odd->text()));
            });

            $p1[]['team'] = $participants;
        });
        echo 'Betclic parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'betclic';
    }
}
