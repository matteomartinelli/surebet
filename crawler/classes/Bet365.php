<?php
namespace Crawler\Bookmaker;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

require_once __DIR__.'/../old/Curl.php';

class Bet365 extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("http://www.bet365.it/home/inplayapi/Sportsbook.asp?lid=6&zid=0&pd=METTI_QUI_IL_PD&wg=0&cid=97&ctid=97");
    }

    public function parse($url){
        $options = array(
            array(
                'type' => 'compressed'
            )
        );
        $headers = array(
            'Pragma: no-cache',
            'Accept-Encoding: gzip, deflate, sdch',
            'Accept-Language: it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4,fr;q=0.2,mt;q=0.2',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36',
            'Accept: */*',
            'Referer: http://www.bet365.it/home/FlashGen4/WebConsoleApp.asp?&cb=105812026996',
            'X-Requested-With: ShockwaveFlash/21.0.0.24',
            'Cookie: cif=0; cp2=0; rmbs=3; aps03=oty=2&cg=0&cst=230&cf=N&ct=97&tzi=4&hd=N&lng=6; pstk=3AB1D9F5FB46C6F59DB481EE54E699E7000003; session=processform=0&id=%7BF2D3DD48%2D6351%2D4AD4%2D97FE%2D28B56B3F3682%7D&psite=1&flslnk=&event=&diary=&fms=&pscp=%23AS%23B1%23',
            'Connection: keep-alive',
            'Cache-Control: no-cache'
        );

        $ch = \Curl::generateCurl("http://www.bet365.it/home/inplayapi/Sportsbook.asp?lid=6&zid=0&pd=%23AS%23B1%23&wg=0&cid=97&ctid=97", $headers, $options);

        $base_content = curl_exec($ch);

        curl_close($ch);



        $p1_total = array();
        $last_index=0;
        do{
            sleep(rand(0,3));

            $index = strpos($base_content, "Palinsesto del", $last_index);
            ob_flush();
            flush();
            if($index === false) break;

            $start = 0;
            $end = 0;

            for($i=$index; $i>0; $i--){
                if($base_content[$i] == "|") {
                    $start = $i;
                    break;
                }
            }

            for($i=$index; $i<strlen($base_content); $i++){
                if($base_content[$i] == "|") {
                    $end = $i;
                    break;
                }
            }

            $last_index = $end;

            $mystr = substr($base_content, $start, $end-$start);
            $slices = explode(";", $mystr);
            $pd = "";
            foreach($slices as $slice){
                if(substr($slice,0,2) == "PD"){
                    $pd = substr($slice,3,strlen($slice));
                    break;

                }
            }

            $pd .= "W^0";
            $options = array(
                array(
                    'type' => 'compressed'
                )
            );
            $headers = array(
                'Pragma: no-cache',
                'Accept-Encoding: gzip, deflate, sdch',
                'Accept-Language: it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4,fr;q=0.2,mt;q=0.2',
                'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36',
                'Accept: */*',
                'Referer: http://www.bet365.it/home/FlashGen4/WebConsoleApp.asp?&cb=105812026996',
                'X-Requested-With: ShockwaveFlash/21.0.0.24',
                'Cookie: cif=0; cp2=0; rmbs=3; pstk=492CF6AFBDAB414E9004DEF5244F321D000003; aps03=oty=2&cg=0&cst=229&tzi=4&cf=N&ct=97&hd=N&lng=6; session=processform=0&fms=&event=&id=%7BA17E5FDF%2DABFE%2D4E3D%2DB499%2DEABF23C0A623%7D&pscp=%23AS%23B1%23&diary=&psite=1&flslnk=',
                'Connection: keep-alive',
                'Cache-Control: no-cache'
            );

            $ch = \Curl::generateCurl(str_replace("METTI_QUI_IL_PD", urlencode($pd), $url), $headers, $options);

            $content = curl_exec($ch);

            curl_close($ch);

            ////// ------------------____> PARTIAMO A PARSARE

            $mg=explode("|MG",$content); //Estraggo ogni campionato nel i=0 c'è merda

            $p1 = array();

            for($i=1;$i<count($mg); $i++){
                $pa=explode("|PA", $mg[$i]);

                $num_partite = (int)((count($pa)-1) / 5);
                for($j=1; $j<=$num_partite;$j++){
                    $commas=explode(";",$pa[$j]);

                    $squadra1 = "";
                    $squadra2 = "";
                    $quota_1 = "";
                    $quota_x = "";
                    $quota_2 = "";
                    //Prendo nomi delle squadre
                    foreach($commas as $key => $value){
                        //TROVA NOMI SQUADRE
                        if(strpos($value,"NA=") !== false){
                            $partite=explode(' v ',substr($value,3,strlen($value)));
                            $squadra1=$partite[0];
                            $squadra2=$partite[1];
                            break;
                        }
                    }

                    $count=0;
                    for($h=$num_partite+$j; $h<count($pa); $h+=$num_partite){
                        $commas=explode(";",$pa[$h]);

                        //prendo quote delle partite
                        foreach($commas as $key => $value){
                            if(strpos($value,"OD=") !== false){
                                if($count == 0){
                                    $quota_1 = $this->convertFractionToDecimal(str_replace("OD=", "", $value));
                                    break;
                                }else if($count == 1){
                                    $quota_x = $this->convertFractionToDecimal(str_replace("OD=", "", $value));
                                    break;
                                }else if($count == 2) {
                                    $quota_2 = $this->convertFractionToDecimal(str_replace("OD=", "", $value));
                                    break 2;
                                }
                            }
                        }
                        $count++;
                    }

                    $p1[]=array(
                        'team' => array(
                            array(
                                'name' => $squadra1,
                                'quota' => $quota_1,
                                'book' => $this->getName()
                            ),
                            array(
                                'name' => 'X',
                                'quota' => $quota_x,
                                'book' => $this->getName()
                            ),
                            array(
                                'name' => $squadra2,
                                'quota' => $quota_2,
                                'book' => $this->getName()
                            )
                        )
                    );
                }
            }

            $p1_total = array_merge($p1_total, $p1);
        }while(true);

        echo 'Bet365 parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1_total;
    }

    public function getName(){
        return 'bet365';
    }
}
