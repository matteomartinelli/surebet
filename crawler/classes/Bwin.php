<?php
namespace Crawler\Bookmaker;
use Goutte\Client;

class Bwin extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("dateFilter=today&sportId=4");
    }

    public function parse($url){
        $client = new Client();
        $p_total = array();

        $i=0;
        $no_more=false;
        while(!$no_more) {
            $page = "&page=".$i++;
            $no_more=false;
            $p1 = array();

            $tmp_url = $url . $page;

            parse_str($tmp_url, $url_parsed);
            $crawler = $client->request('POST', 'https://sports.bwin.it/it/sports/indexmultileague', $url_parsed);
            /*$crawler->filter('.marketboard > .ui-widget-content > .ui-widget-content-body > .marketboard-event-group > .marketboard-event-group__item-container > .marketboard-event-group__item--sub-group > .marketboard-event-group__item-container > .marketboard-event-group__item--sub-group > .marketboard-event-group__item-container > .marketboard-event-group__item--event')->each(function ($node) use (&$p1){
                die(var_dump($node->html()));

                /*$node->filter('.marketboard-event-group__item-container')->each(function ($node2) use (&$p1){
                })

            });*/
            $crawler->filter('.marketboard')->each(function ($node) use (&$p1){
                $node->filter('.marketboard-event-without-header')->each(function ($node2) use (&$p1){
                    $p1[] = array();
                    $index = count($p1) -1;

                    $node2->filter('.mb-option-button')->each(function($node_tmp) use (&$p1, &$index){
                        $name = $node_tmp->filter(".mb-option-button__option-name")->text();
                        $quota = $node_tmp->filter(".mb-option-button__option-odds")->text();
                        //$quota=$this->convertFractionToDecimal($quota);
                        if(!isset($p1[$index]['team']) or !is_array($p1[$index]['team'])) $p1[$index]['team'] = array();
                        array_push($p1[$index]['team'], array('name'=> $name, 'quota' => $quota, 'book' => $this->getName()));
                    });

                });
            });

            if($i > 10){
                break;
            }

            $p_total = array_merge($p_total, $p1);
        }

        echo 'Bwin parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p_total;
    }

    public function getName(){
        return 'bwin';
    }
}