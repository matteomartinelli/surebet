<?php
namespace Crawler\Bookmaker;
use Goutte\Client;

class Marathon extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("https://www.marathonbet.com/it/period-events.htm?periodgroup=24");
    }

    public function parse($url){
        $p1 = array();

        $client = new Client();
        $crawler = $client->request('GET', $url);

        $table = null;

        $crawler->filter('#container_EVENTS > .sport-category-container')->each(function ($node) use (&$p1){
            $node->filter('.sport-category-label')->each(function ($node_first) use ($node, &$p1){
                if(strtolower($node_first->text()) == 'football'){
                    $node->filter('.foot-market')->each(function ($table_node) use (&$p1){
                        $table_node->filter('tbody')->each(function ($node4) use (&$p1){
                            $node4->filter('tr')->each(function ($node5) use (&$p1){
                                $count = 0;


                                $tds = explode("</td>", $node5->html());

                                $teams = explode("2.", str_replace("1.", "", strip_tags($tds[0])));

                                if(count($tds)-4 > 0){
                                    $p1[] = array(
                                        'team' => array(
                                            array(
                                                'name' => trim($teams[0]),
                                                'quota' => (float)trim(strip_tags($tds[count($tds)-4])),
                                                'book' => $this->getName()
                                            ),
                                            array(
                                                'name' =>'X',
                                                'quota' => (float)trim(strip_tags($tds[count($tds)-3])),
                                                'book' => $this->getName()
                                            ),
                                            array(
                                                'name' => trim($teams[1]),
                                                'quota' => (float)trim(strip_tags($tds[count($tds)-2])),
                                                'book' => $this->getName()
                                            )
                                        )
                                    );
                                }


                            });
                        });
                    });
                }
            });
        });

        echo 'Marathon parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'marathon';
    }
}