<?php
namespace Crawler\Bookmaker;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

require_once __DIR__.'/../old/Curl.php';
class Tipico extends Bookmaker implements BookmakerInterface
{
    public $jsession='';
    public function __construct($cookie){
        $this->jsession=$cookie;
        $this->parse("https://www.tipico.it/spring/complete/www_tipico_it_it_scommesse_sportive_online_calcio_g1101_30");


    }

    public function parse($url){
        $p1 = array();
        $team1='';
        $team2='';
        $odd1=0;
        $oddx=0;
        $odd2=0;
        $n=0;

        //first
        ///sports/selection/setTimePeriod%3FperiodIndex%3D2
        //after
        //
        //cicla solo 5 pagine
        for($i=1; $i<=5;$i++){
            $options = array(
                array(
                    'type' => 'compressed'
                ),
                array(
                    'type' => 'data'
                ),
                array(
                    'type' => 'payload_post',
                    'value_' => serialize(array(
                        '__string__' => ($i==1) ? '_=/sports/selection/setTimePeriod%3FperiodIndex%3D2' : '_=@selection/sports/selection/selection%3Fpage%3D'.$i
                    ))
                )
            );

            $headers = array(
                'Pragma: no-cache',
                'Origin: https://www.tipico.it',
                'Accept-Encoding: gzip, deflate, br',
                'hash-url: ',
                'Accept-Language: it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4,fr;q=0.2,mt;q=0.2',
                'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36',
                'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                'Accept: text/html, */*; q=0.01',
                'X-Requested-With: XMLHttpRequest',
                'Cookie: JSESSIONID='. $this->jsession .';',
                'Connection: keep-alive',
                'Referer: https://www.tipico.it/it/scommesse-sportive-online/calcio/g1101/',
                'Cache-Control: no-cache'
            );

            $ch = \Curl::generateCurl($url, $headers, $options);

            $base_content = curl_exec($ch);

            curl_close($ch);

            $crawler = new Crawler();
            $crawler->addContent($base_content);
            //die(var_dump($crawler->html()));

            $participants = array();
            $crawler->filter('.e_active.t_row')->each(function ($match) use (&$p1, &$team1, &$team2, &$odd1, &$oddx, &$odd2, &$n ){
                $n=0;
                $match->filter('.t_cell.w_128.left')->each(function($team) use (&$n, &$team1, &$team2){
                    if($n==0) $team1 = $team->text();
                    if($n==1) $team2 = $team->text();
                    $n=$n+1;
                });
                $n=0;
                $match->filter('.bl.br.left.cf .qbut')->each(function($odd) use (&$n, &$odd1, &$oddx, &$odd2){
                    if($n==0) $odd1 = floatval(str_replace(',','.',$odd->text()));
                    if($n==1) $oddx = floatval(str_replace(',','.',$odd->text()));
                    if($n==2) $odd2 = floatval(str_replace(',','.',$odd->text()));
                    $n=$n+1;
                });
                if(trim($team1) != ""){
                    $p1[] = array(
                        'team' => array(
                            array(
                                'name' => trim($team1),
                                'quota' => $odd1,
                                'book' => $this->getName()
                            ),
                            array(
                                'name' =>'X',
                                'quota' => $oddx,
                                'book' => $this->getName()
                            ),
                            array(
                                'name' => trim($team2),
                                'quota' => $odd2,
                                'book' => $this->getName()
                            )
                        )
                    );
                }
            });
        }

        echo 'Tipico parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'tipico';
    }
}