<?php
namespace Crawler\Bookmaker;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Eurobet extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("http://web.eurobet.it/webeb/sport?action=scommesseV2_today_comm&dayMode=-2&chooseSport=1&showSplash=0&ts=". time());
    }

    public function parse($url){
        $client = new Client();

        parse_str($url, $url_parsed);

        $crawler = $client->request('GET', $url);

        $p1 = array();
        $crawler->filter('.box_container_scommesse_evento')->each(function ($node) use (&$p1){
            $participants = array();
            $node->filter('.box_container_scommesse_nomeEvento')->each(function($node_match) use (&$participants){
                $tmp = explode("-", $node_match->text());
                $participants[] = array(
                    'name' => trim($tmp[0]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => "X",
                    'quota' => 0,
                    'book' => $this->getName()
                );
                $participants[] = array(
                    'name' => trim($tmp[1]),
                    'quota' => 0,
                    'book' => $this->getName()
                );
            });

            $i=0;
            $node->filter(".box_container_scommesse_quote > .box_container_scommesse_quoteType")->each(function($node_odd) use (&$participants, &$i){
                if($i==3) return;
                $participants[$i++]['quota'] = (float)trim(str_replace(",",".", $node_odd->text()));
            });

            $p1[]['team'] = $participants;
        });
        echo 'Eurobet parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'eurobet';
    }
}
