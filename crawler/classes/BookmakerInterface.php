<?php
namespace Crawler\Bookmaker;

interface BookmakerInterface
{
    public function parse($url);

    public function getMatches();

    public function getName();
}