<?php
namespace Crawler\Bookmaker;

class Bookmaker
{
    protected $matches = array();

    public static function convertFractionToDecimal($quote){
        if(strpos($quote,'/')){
            $frazione=explode('/',$quote);
            $numeratore= $frazione[0] ;
            $denominatore= $frazione[1];
            $quote= ($numeratore/$denominatore) + 1;
        }
        return $quote;
    }

    public static function convertAmericanToDecimal($quota){
        $sign = substr($quota, 0,1);
        $quota = str_replace(array("-","+"), "", $quota);
        if($sign != "-"){
            $quota = ($quota/100) +1;
        }else{
            $quota = (100/$quota) +1;
        }

        return $quota;
    }

    public function getMatches(){
        return $this->matches;
    }
}