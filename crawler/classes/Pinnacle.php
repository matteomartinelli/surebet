<?php
namespace Crawler\Bookmaker;
use Goutte\Client;

class Pinnacle extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("http://www.pinnacle.com/webapi/1.15/api/v1/GuestLines/Today/29");
    }

    public function parse($url){
        $p2 = array();
        $client = new Client();
        $crawler = $client->request('GET', $url, array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
            ));
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        foreach ($data['Leagues'] as $league){
            foreach ($league['Events'] as $match) {
                if(!isset($p2[$match['EventId']])){
                    $p2[$match['EventId']] = array();
                    $index = $match['EventId'];
                    $inserted = true;
                    $i=1;

                    foreach ($match['Participants'] as $participant) {
                        if($participant['MoneyLine'] == null){
                            $inserted = false;
                            break;
                        }

                        if($i==1){
                            $c=0;
                        }elseif($i==2){
                            $c=2;
                        }else{
                            $c=1;
                        }
                        $i++;
                        $p2[$index]['team'][$c] = array(
                            'name' => ($participant['Name'] != null) ? $participant['Name'] : "X",
                            'quota' => (substr($participant['MoneyLine'], 0,1) == "-") ?
                                (100/substr($participant['MoneyLine'], 1,strlen($participant['MoneyLine']))) +1 :
                                ($participant['MoneyLine']/100) +1,
                            'book' => $this->getName()
                        );
                    }

                    $p2[$match['EventId']]['time'] = $match['DateAndTime'];

                    if(!$inserted){
                        unset($p2[$index]);
                    }
                }
            }
        }
        echo 'Pinnacle parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p2;
    }

    public function getName(){
        return 'pinnacle';
    }
}