<?php
namespace Crawler\Bookmaker;
use Goutte\Client;

class _888 extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("");
    }

    public function parse($url){
        $p1 = array();
        $start = 0;

        do{
            $client = new Client();
            /*$crawler = $client->request('GET', 'https://e4-api.kambi.com/offering/api/v2/888it/betoffer/main/timespan/24.json?categoryGroup=pre_match_league&lang=it_IT&market=it&range_size=100&range_start='.$start.'&excluded_sport=GALLOPS&excluded_sport=GREYHOUNDS&suppress_response_codes&channel_id=1', array(),
                array(),
                array(
                    'CONTENT_TYPE' => 'application/json',
                ));*/
            $crawler = $client->request('GET', 'https://e2-api.kambi.com/offering/api/v2/888it/betoffer/group/1000093190.json?cat=1295&lang=it_IT&market=it&range_size=100&range_start='.$start.'&suppress_response_codes&channel_id=1', array(),
                array(),
                array(
                    'CONTENT_TYPE' => 'application/json',
                ));

            $response = $client->getResponse();
            $data = json_decode($response->getContent(), true);

            foreach ($data['betoffers'] as $match){
                $participants=array();
                foreach ($match['outcomes'] as $odds) {
                    $participants[] = array(
                        'name' => $odds['label'],
                        'quota' => round($odds['odds']/1000,2),
                        'book' => $this->getName()
                    );
                }
                $p1[$match['eventId']]['team'] =$participants;

            }

            foreach ($data['events'] as $match){
                $p1[$match['id']]['team']['0']['name']=$match['homeName'];
                $p1[$match['id']]['team']['2']['name']=$match['awayName'];
            }
            $start = $data['range']['start'] + $data['range']['size'];
        }while($data['range']['start'] + $data['range']['size'] < $data['range']['total']);
        //start=0 //size=100 //total=883
        echo '888 parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return '_888';
    }
}