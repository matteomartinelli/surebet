<?php
namespace Crawler\Bookmaker;

class Comparator
{

    public function compare($bookmakers){
        //die(var_dump(json_encode($bookmakers)));
        if(count($bookmakers) <= 0) return array();

        $matches = $bookmakers[0];

        for($i=1; $i<count($bookmakers); $i++){
            $matches = $this->compare2($matches, $bookmakers[$i]);
        }

        return $matches;
    }

    private function compare2($book1, $book2){
        $p2_matched = array();
        $matched = array();

        foreach ($book1 as $key1 => $match_1) {
            $found = false;
            foreach ($book2 as $key2 =>  $match_2) {
                if(!in_array($key2, $p2_matched)){
                    similar_text(strtolower($match_1['team'][0]['name']), strtolower($match_2['team'][0]['name']), $sim1);
                    similar_text(strtolower($match_1['team'][1]['name']), strtolower($match_2['team'][1]['name']), $simx);
                    similar_text(strtolower($match_1['team'][2]['name']), strtolower($match_2['team'][2]['name']), $sim2);

                    if( ($sim1 > 55 and $sim2 > 55) or ($sim1>80 and $sim2 > 35) or ($sim1>35 and $sim2>80)){
                        $found = true;
                        $matched[] = array(
                            'team' => array(
                                array(
                                    'name' => $match_1['team'][0]['name'],
                                    'quota' => ($match_1['team'][0]['quota'] >= $match_2['team'][0]['quota']) ? $match_1['team'][0]['quota'] : $match_2['team'][0]['quota'],
                                    'book' => ($match_1['team'][0]['quota'] >= $match_2['team'][0]['quota']) ? $match_1['team'][0]['book'] : $match_2['team'][0]['book'],
                                ),
                                array(
                                    'name' => $match_1['team'][1]['name'],
                                    'quota' => ($match_1['team'][1]['quota'] >= $match_2['team'][1]['quota']) ? $match_1['team'][1]['quota'] : $match_2['team'][1]['quota'],
                                    'book' => ($match_1['team'][1]['quota'] >= $match_2['team'][1]['quota']) ? $match_1['team'][1]['book'] : $match_2['team'][1]['book'],
                                ),
                                array(
                                    'name' => $match_1['team'][2]['name'],
                                    'quota' => ($match_1['team'][2]['quota'] >= $match_2['team'][2]['quota']) ? $match_1['team'][2]['quota'] : $match_2['team'][2]['quota'],
                                    'book' => ($match_1['team'][2]['quota'] >= $match_2['team'][2]['quota']) ? $match_1['team'][2]['book'] : $match_2['team'][2]['book'],
                                )
                            ),
                            'time' => "",
                        );

                        $p2_matched[] = $key2;
                        break;
                    }
                }
            }
            if(!$found){
                $matched[] = $match_1;
            }
        }

        foreach($book2 as $key2 => $match_2){
            if(!in_array($key2, $p2_matched)){
                //lo aggiungo perchè non ha trovato nessuna similarità con book1
                $matched[] = $match_2;
            }
        }

        return $matched;
    }
}