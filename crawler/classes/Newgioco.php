<?php
namespace Crawler\Bookmaker;
use Goutte\Client;

class Newgioco extends Bookmaker implements BookmakerInterface
{
    public function __construct(){
        $this->parse("From=". date("d/m/Y") ."&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    }

    public function parse($url){
        $p1 = array();
        $start = 0;

        do{
            $client = new Client();
            /*$crawler = $client->request('GET', 'https://e4-api.kambi.com/offering/api/v2/ubit/betoffer/main/timespan/24.json?categoryGroup=pre_match_league&lang=it_IT&market=it&range_size=100&range_start='.$start.'&excluded_sport=GALLOPS&excluded_sport=GREYHOUNDS&suppress_response_codes&channel_id=1', array(),
                array(),
                array(
                    'CONTENT_TYPE' => 'application/json',
                ));*/
            $crawler = $client->request('GET', 'https://www.newgioco.it/betmg_fullscreen.aspx?html5=1&html5=1&aliaspath=%2fwebsite%2fbetting%2fbetMgNode%2fbetMg_fullScreen', array(),
                array(),
                array(
                    'CONTENT_TYPE' => 'application/json',
                ));

            die($client->getResponse());
            $response = $client->getResponse();
            $data = json_decode($response->getContent(), true);


            foreach ($data['betoffers'] as $match){
                $participants=array();
                foreach ($match['outcomes'] as $odds) {
                    $participants[] = array(
                        'name' => $odds['label'],
                        'quota' => round($odds['odds']/1000,2),
                        'book' => $this->getName()
                    );
                }
                $p1[$match['eventId']]['team'] =$participants;

            }

            foreach ($data['events'] as $match){
                $p1[$match['id']]['team']['0']['name']=$match['homeName'];
                $p1[$match['id']]['team']['2']['name']=$match['awayName'];
            }
            $start = $data['range']['start'] + $data['range']['size'];
        }while($data['range']['start'] + $data['range']['size'] < $data['range']['total']);
        //start=0 //size=100 //total=883
        echo 'Unibet parsed<br>';
        ob_flush();
        flush();
        $this->matches = $p1;
    }

    public function getName(){
        return 'unibet';
    }
}