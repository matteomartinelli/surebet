<style>
    h1,h2,h3,h4,h5,h6{
        margin: 0;
    }
    table,th,tr,td{
        border: 1px #000 solid;
    }
    th{
        font-weight: bolder;
        font-size: 25px;
    }
    tr{
        height: 40px;
    }
    td {
        padding: 10px;
    }

</style>

<form method="post" action="?action=compute">
    Quanto giochi su Isibet? <input type=text name="quanto"><br>
    <input type="submit" value="computa">
</form>

<?php
use Crawler\Bookmaker\Pinnacle;
use Crawler\Bookmaker\Isibet;
use Crawler\Bookmaker\Comparator;

require_once 'classes/Bookmaker.php';
require_once 'old/Curl.php';
require_once 'classes/BookmakerInterface.php';
require_once 'classes/Isibet.php';
require_once 'classes/Pinnacle.php';
require_once 'classes/Comparator.php';

require_once 'vendor/autoload.php';

if(isset($_GET['action'])){
    $p1 = array();
    $p2 = array();
    $matched = array();
    $quanto=($_POST['quanto'] == "") ? 100 : $_POST['quanto'];
    $sureColor="";

    $Isibet = new Isibet();
    $p1 = $Isibet->getMatches();

    $pinnacle = new Pinnacle();
    $p2 = $pinnacle->getMatches();

    $quota_da_giocare = 1.5;
    $name_bookmaker = $Isibet->getName();

    $p2_matched = array();
    foreach ($p1 as $key1 => $match_1) {
        foreach ($p2 as $key2 =>  $match_2) {
            if(!in_array($key2, $p2_matched)){
                similar_text(strtolower($match_1['team'][0]['name']), strtolower($match_2['team'][0]['name']), $sim1);
                similar_text(strtolower($match_1['team'][1]['name']), strtolower($match_2['team'][1]['name']), $simx);
                similar_text(strtolower($match_1['team'][2]['name']), strtolower($match_2['team'][2]['name']), $sim2);

                if( ($sim1 > 55 and $sim2 > 55) or ($sim1>80 and $sim2 > 35) or ($sim1>35 and $sim2>80)){
                    $tmp_matched = array(
                        'time' => $match_2['time'],
                        'p1' => $match_1,
                        'p2' => $match_2
                    );


                    //QUOTE MIGLIORI
                    $quota_1 = ($match_1['team'][0]['quota'] >= $match_2['team'][0]['quota']) ? $match_1['team'][0]['quota'] : $match_2['team'][0]['quota'];
                    $quota_1_book = ($match_1['team'][0]['quota'] >= $match_2['team'][0]['quota']) ? $match_1['team'][0]['book'] : $match_2['team'][0]['book'];

                    $quota_x = ($match_1['team'][1]['quota'] >= $match_2['team'][1]['quota']) ? $match_1['team'][1]['quota'] : $match_2['team'][1]['quota'];
                    $quota_x_book = ($match_1['team'][1]['quota'] >= $match_2['team'][1]['quota']) ? $match_1['team'][0]['book'] : $match_2['team'][0]['book'];

                    $quota_2 = ($match_1['team'][2]['quota'] >= $match_2['team'][2]['quota']) ? $match_1['team'][2]['quota'] : $match_2['team'][2]['quota'];
                    $quota_2_book = ($match_1['team'][2]['quota'] >= $match_2['team'][2]['quota']) ? $match_1['team'][0]['book'] : $match_2['team'][0]['book'];

                    $quanto_1=0;
                    $quanto_x=0;
                    $quanto_2=0;

                    if($quota_1_book==$name_bookmaker && $quota_1>=$quota_da_giocare ){
                        $quanto_1=$quanto;
                        $quanto_x=($quanto_1*$quota_1)/$quota_x;
                        $quanto_2=($quanto_1*$quota_1)/$quota_2;

                    }elseif($quota_x_book==$name_bookmaker && $quota_x>=$quota_da_giocare ){
                        $quanto_x=$quanto;
                        $quanto_1=($quanto_x*$quota_x)/$quota_1;
                        $quanto_2=($quanto_x*$quota_x)/$quota_2;
                    }elseif($quota_2_book==$name_bookmaker && $quota_2>=$quota_da_giocare ){
                        $quanto_2=$quanto;
                        $quanto_1=($quanto_2*$quota_2)/$quota_1;
                        $quanto_x=($quanto_2*$quota_2)/$quota_x;
                    }

                    $tmp_matched['best'] = array(
                        array(
                            'quanto' => $quanto_1,
                            'quota' => $quota_1,
                            'book' => $quota_1_book
                        ),
                        array(
                            'quanto' => $quanto_x,
                            'quota' => $quota_x,
                            'book' => $quota_x_book
                        ),
                        array(
                            'quanto' => $quanto_2,
                            'quota' => $quota_2,
                            'book' => $quota_2_book
                        )
                    );

                    //CALCOLO LA RESA
                    $tmp_matched['yield'] = 100*(1-(1/$quota_1 + 1/$quota_x + 1/$quota_2));

                    //CALCOLO IL COLORE
                    $color='white';
                    if($quota_1_book == $name_bookmaker and $quota_1 >= $quota_da_giocare){
                        $color = 'green';
                    }else if($quota_x_book == $name_bookmaker and $quota_x >= $quota_da_giocare){
                        $color = 'green';
                    }else if($quota_2_book == $name_bookmaker and $quota_2 >= $quota_da_giocare){
                        $color = 'green';
                    }
                    $tmp_matched['color'] = $color;
                    //CALCOLO PERDITA
                    $tmp_matched['lost'] = number_format(($quanto_1+$quanto_x+$quanto_2)*$tmp_matched['yield']/100, 2);
                    $tmp_matched['total'] = number_format($quanto_1+$quanto_x+$quanto_2, 2 );


                    $matched[] = $tmp_matched;
                    $p2_matched[] = $key2;
                    break;
                }
            }
        }
    }

    // Obtain a list of columns
    $lost = array();
    foreach ($matched as $key => $row) {
        $lost[$key]  = $row['lost'];
    }

    array_multisort($lost, SORT_ASC, $matched);



    echo '<h1>MATCHED</h1>';
    echo "<table>";
    echo "<tr>";
    echo "<th>Ora</th>";
    echo "<th>Isibet</th>";
    echo "<th>Pinnacle</th>";
    echo "<th>1</th>";
    echo "<th>X</th>";
    echo "<th>2</th>";
    echo "<th>Resa</th>";
    echo "<th>Perdita</th>";
    echo "</tr>";
    foreach($matched as $match){
        $sureColor='';
        if($match['yield']>0) $sureColor="style='background-color:#06D'";
        date_default_timezone_set('Europe/London');
        $dt = new DateTime($match['time']);
        $dt = date("d-m-Y H:i", $dt->getTimestamp() + 8*3600);
        echo "<tr style='background-color: ".$match['color']."'>";
        echo "<td>". $dt . "</td>";
        echo "<td>". $match['p1']['team'][0]['name'] . " - " . $match['p1']['team'][2]['name'] . "</td>";
        echo "<td>". $match['p2']['team'][0]['name'] . " - " . $match['p2']['team'][2]['name'] . "</td>";
        echo "<td>" . round($match['best'][0]['quota'],4)."-".$match['best'][0]['book']. "- ". round($match['best'][0]['quanto'],2) ."</td>";
        echo "<td>" . round($match['best'][1]['quota'],4)."-".$match['best'][1]['book']. "- ". round($match['best'][1]['quanto'],2) ."</td>";
        echo "<td>" . round($match['best'][2]['quota'],4)."-".$match['best'][2]['book']. "- ". round($match['best'][2]['quanto'],2) ."</td>";
        echo "<td ".$sureColor."><h3>" . number_format($match['yield'], 2) . "%</h3></td>";
        echo "<td ".$sureColor."><h3>" . $match['lost'] . "€ (tot: ".$match['total']."€)</h3></td>";
        echo "</tr>";
    }
    echo "</table>";
}

