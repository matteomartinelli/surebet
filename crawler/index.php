<style>
    h1,h2,h3,h4,h5,h6{
        margin: 0;
    }
    table,th,tr,td{
        border: 1px #000 solid;
    }
    th{
        font-weight: bolder;
        font-size: 25px;
    }
    tr{
        height: 40px;
    }
    td {
        padding: 10px;
    }
</style>
<form method="POST" action="?action=compute">
    <input type="submit" value="Computa cross-match">
</form>

<?php
use Crawler\Bookmaker\Bet365;
use Crawler\Bookmaker\Betclic;
use Crawler\Bookmaker\Bwin;
use Crawler\Bookmaker\Comparator;
use Crawler\Bookmaker\GiocoDigitale;
use Crawler\Bookmaker\Isibet;
use Crawler\Bookmaker\Eurobet;
use Crawler\Bookmaker\Pinnacle;
use Crawler\Bookmaker\Unibet;
use Crawler\Bookmaker\WilliamHill;
use Crawler\Bookmaker\Marathon;

require_once 'classes/BookmakerInterface.php';
require_once 'classes/Bookmaker.php';
require_once 'classes/Betclic.php';
require_once 'classes/Bet365.php';
require_once 'classes/Bwin.php';
require_once 'classes/Comparator.php';
require_once 'classes/GiocoDigitale.php';
require_once 'classes/Eurobet.php';
require_once 'classes/Isibet.php';
require_once 'classes/Pinnacle.php';
require_once 'classes/Unibet.php';
require_once 'classes/Marathon.php';
require_once 'classes/WilliamHill.php';

require_once 'vendor/autoload.php';

if(isset($_GET['action'])){
    //$bet365 = new Bet365("http://www.bet365.it/home/inplayapi/Sportsbook.asp?lid=6&zid=0&pd=METTI_QUI_IL_PD&wg=0&cid=97&ctid=97");
    $betclic = new Betclic("From=". date("d/m/Y")."&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    $bwin = new Bwin("dateFilter=today&sportId=4");
    $eurobet = new Eurobet("http://web.eurobet.it/webeb/sport?action=scommesseV2_today_comm&dayMode=-2&chooseSport=1&showSplash=0&ts=" . time());
    $giocodigitale = new GiocoDigitale("dateFilter=today&sportId=4");
    //$isibet = new Isibet("From=". date("d/m/Y")."&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    $marathon = new Marathon("https://www.marathonbet.com/it/period-events.htm?periodgroup=24");
    $pinnacle = new Pinnacle("http://www.pinnacle.com/webapi/1.15/api/v1/GuestLines/Today/29");
    $unibet = new Unibet("From=". date("d/m/Y")."&SortBy=Date&Live=false&MultipleBoost=false&Competitions.Selected=1-0&StartIndex=0");
    $william_hill = new WilliamHill("http://sports.williamhill.it/bet_ita/it/betting/y/5/tm/0/Calcio.html");

    $comparator = new Comparator();
    $all = $comparator->compare(array(
        //$bet365->getMatches(),
        $betclic->getMatches(),
        $bwin->getMatches(),
        $eurobet->getMatches(),
        $giocodigitale->getMatches(),
        //$isibet->getMatches(),
        $marathon->getMatches(),
        $pinnacle->getMatches(),
        $unibet->getMatches(),
        $william_hill->getMatches()
    ));

    /*$string = file_get_contents("stub.json");
    $matches = json_decode($string, true);

    $comparator = new Comparator();
    $all = $comparator->compare($matches);*/

    echo '<h1>MATCHED</h1>';
    echo "<table>";
    echo "<tr>";
    echo "<th>Ora</th>";
    echo "<th>Partita</th>";
    echo "<th>1</th>";
    echo "<th>X</th>";
    echo "<th>2</th>";
    echo "<th>Resa</th>";
    echo "</tr>";
    foreach($all as $match){
        $resa=100*(1-(1/$match['team'][0]['quota'] + 1/$match['team'][1]['quota'] + 1/$match['team'][2]['quota']));

        $color='white';
        $sureColor="";
        if($resa>0) $sureColor="style='background-color:#06D'";

        if(isset($match['time'])){
            date_default_timezone_set('Europe/London');
            $dt = new DateTime($match['time']);
            $dt = date("d-m-Y H:i", $dt->getTimestamp() + 8*3600);
        }else{
            $dt = "(not identified)";
        }

        echo "<tr style='background-color: ".$color."'>";
        echo "<td>". $dt . "</td>";
        echo "<td>". $match['team'][0]['name'] . " - " . $match['team'][2]['name'] . "</td>";
        echo "<td>" . round($match['team'][0]['quota'],4)."(".$match['team'][0]['book']. ")</td>";
        echo "<td>" . round($match['team'][1]['quota'],4)."(".$match['team'][1]['book']. ")</td>";
        echo "<td>" . round($match['team'][2]['quota'],4)."(".$match['team'][2]['book']. ")</td>";
        echo "<td ".$sureColor."><h3>" . $resa . "%</h3></td>";
        echo "</tr>";
    }
    echo "</table>";
}