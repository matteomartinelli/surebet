<?php

class Curl {

    const BETTER = 1;
    const SISAL = 2;
    const SNAI = 3;
    const BETCLIC = 4;
    const UNIBET = 5;
    const INTERWETTEN = 6;
    const WILLIAMHILL = 7;
    const PINNACLE = 8;
    const TIPICO = 9;
    const IZI = 10;
    const BET365 = 11;
    const BWIN = 12;
    const BETFLAG = 13;
    const MARATHON = 14;
    const EUROBET = 15;
    const GOLDBET = 16;
    const _10BET = 17;

    public static function getBookmaker($id){
        switch($id){
            case Bookmaker::BETTER:
                return 'BETTER';
                break;
            case Bookmaker::SISAL:
                return 'SISAL';
                break;
            case Bookmaker::SNAI:
                return 'SNAI';
                break;
            case Bookmaker::BETCLIC:
                return 'BETCLIC';
                break;
            case Bookmaker::UNIBET:
                return 'UNIBET';
                break;
            case Bookmaker::INTERWETTEN:
                return 'INTERWETTEN';
                break;
            case Bookmaker::WILLIAMHILL:
                return 'WILLIAMHILL';
                break;
            case Bookmaker::PINNACLE:
                return 'PINNACLE';
                break;
            case Bookmaker::TIPICO:
                return 'TIPICO';
                break;
            case Bookmaker::IZI:
                return 'IZI';
                break;
            case Bookmaker::BET365:
                return 'BET365';
                break;
            case Bookmaker::BWIN:
                return 'BWIN';
                break;
            case Bookmaker::BETFLAG:
                return 'BETFLAG';
                break;
            case Bookmaker::MARATHON:
                return 'MARATHON';
                break;
            case Bookmaker::EUROBET:
                return 'EUROBET';
                break;
            case Bookmaker::GOLDBET:
                return 'GOLDBET';
                break;
            case Bookmaker::_10BET:
                return '10BET';
                break;
        }

        return false;
    }

    public static function generateCurl($url, $db_headers, $db_options){
        $ch = curl_init();

        //Setto url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING , "gzip");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //Setto header
        $headers=array();
        foreach( $db_headers as $header)
            array_push($headers, $header);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //Setto options
        foreach( $db_options as $option){
            switch($option['type']){
                case 'data-binary':
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE); // --data-binary
                    break;
                case 'data':
                    curl_setopt($ch, CURLOPT_TRANSFERTEXT, TRUE); // --data
                    break;
                case '-X POST':
                    curl_setopt($ch, CURLOPT_POST, 1); // -X POST
                    break;
                case 'payload_post':
                    $value = unserialize($option['value_']);
                    if(!array_key_exists('__string__',$value)){
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($value));
                    }else{
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $value['__string__']);
                    }
                    break;
                case 'compressed':
                    curl_setopt($ch, CURLOPT_VERBOSE, TRUE); // --compressed
                    break;
                case '-X PUT':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); // -X PUT
                    break;
                case '-0':
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); // -0
                    break;

            }
        }

        return $ch;
    }
}