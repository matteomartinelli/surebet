<?php

class Config {
    public static $database = "surebet";
    public static $host = "localhost";
    public static $username = "root";
    public static $password = "root";
    public static $port = "";

    private $db;

    public function __construct(){
        $this->setDatabase();
    }

    public function setDatabase(){
        $this->db =  mysqli_connect(Config::$host, Config::$username, Config::$password, Config::$database);
    }

    public function getDatabase(){
        return $this->db;
    }

    public function getDb(){
        return $this->db;
    }
}