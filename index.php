<?php

?>

<html>
<head>
	<title>SUREBET index</title>
	<script src="js/jquery-2.1.4.min.js"></script>

	<link href="css/bootstrap.min.css" rel="stylesheet">

	<style>
		#log > div{
			margin-bottom: 15px;
			padding-bottom: 15px;
			border-bottom: 1px solid #ccc;
		}
	</style>
</head>

<body>
<h1>Index</h1>
<ul>
	<li>
		<a href="bot.php" style="font-size: 19px;">Avvia il bot</a>
	</li>
	<li>
		<a href="book.php" style="font-size: 19px;">Bookmaker attivi</a>
	</li>
	<li>
		<a href="add.php" style="font-size: 19px;">Aggiungi cURL</a>
	</li>
	<li>
		<a href="duplicate.php" style="font-size: 19px;">Duplica cURL</a>
	</li>
	<li>
		<a href="list.php" style="font-size: 19px; font-weight: bold">Lista match sure</a>
	</li>
	<li>
		-----------------------------
	</li>
	<li>
		<a href="test/test_retrieve.php" style="font-size: 19px;">Test retrieve <span class="small">(Prova tutte le curl attive, indipendentemente dal campionato)</span></a>
	</li>
	<li>
		<a target="_blank" href="/phpmyadmin" style="font-size: 19px;">Phpmyadmin</a>
	</li>
</ul>
</body>
</html>
