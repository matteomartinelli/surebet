1 - installare xampp/lamp:
	
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install mysql-server php5-mysql
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
sudo service apache2 restart

poi phpmyadmin per gestire il db
sudo apt-get install phpmyadmin apache2-utils

nel wizard seleziona come seguito
Select Apache2 for the server
Choose YES when asked about whether to Configure the database for phpmyadmin with dbconfig-common
Enter your MySQL password when prompted (metti root)
Enter the password that you want to use to log into phpmyadmin (root)

sudo nano /etc/apache2/apache2.conf

in fondo a sto file aggiungi:
Include /etc/phpmyadmin/apache.conf


a questo punto se vai su localhost/phpmyadmin e metti root root puoi gestire il database.

-----------
da terminale
cd /var/www/
nautilus .

a questo punto si apre la finestra di navigazione, copia e incolla la cartella surebet

prima di andare avanti andare su localhost/phpmyadmin, creare il database surebet e importare li dentro il file che si trova in doc/surebet.sql.

ora puoi andare su
localhost/surebet/index.php - questa è la pagina da aprire
localhost/surebet/list.php - per vedere la lista delle partite sicure
localhost/surebet/retrieve.php - non ti serve
localhost/surebet/save.php - non ti serve

a questo punto nella cartella js/parser/ ci sono tutti i parser da fare. quello che riceve il parse è una stringa (che puo essere html o json) e deve restituire un array (dobbiamo trovare uno standard) con dentro tutti i match che ha trovato.

per scrivere la funzione parse_better si utilizza jquery.
