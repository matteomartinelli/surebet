<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
include_once "php/Config.php";
include_once "php/Bookmaker.php";

$return = array();
$cnf = new Config();


if(isset($_GET['action']) and isset($_GET['id'])){
	if($_GET['action'] == 'active'){
		$query = "UPDATE curl SET active = 1 WHERE cid = " . $_GET['id'];
		$exec_global = mysqli_query($cnf->getDb(), $query);
	}else{
		$query = "UPDATE curl SET active = 0 WHERE cid = " . $_GET['id'];
		$exec_global = mysqli_query($cnf->getDb(), $query);

	}
}


$tables = array(
	'',
	'',
	'',
	''
);


$query = "SELECT L.*, C.cid, C.bid, C.active FROM league AS L JOIN curl as C ON L.lid = C.lid ORDER BY L.lid, C.bid ASC";
$exec_global = mysqli_query($cnf->getDb(), $query);

$i = 0;
$lid = -1;
$first = true;
while ( $bookmaker = mysqli_fetch_assoc($exec_global)) {
	if($first) {
		$lid = $bookmaker['lid'];
		$tables[$i] .= '<h3>'.$bookmaker['name'].'</h3><table>';
		$first = false;
	}

	if($bookmaker['lid'] != $lid){
		$lid = $bookmaker['lid'];
		$i = ($i + 1) % 4;
		if($tables[$i] != '') $tables[$i] .= '</table>';
		$tables[$i] .= '<h3>'.$bookmaker['name'].'</h3><table>';
	}
	if($bookmaker['active'] == 1){
		$name = "Disattiva";
		$class="btn-danger";
		$pre="dis";
	}else{
		$name = "Attiva";
		$class="btn-success";
		$pre="";
	}

	$tables[$i] .= '<tr><td>'.Bookmaker::getBookmaker($bookmaker['bid']).' <a href="book.php?action='.$pre.'active&id='.$bookmaker['cid'].'" class="btn btn-xs '.$class.'">'.$name.'</a></td></tr>';
}

for ( $i=0; $i<4; $i++){
	if($tables[$i] != "") $tables[$i] .= '</table>';
}
?>

<html>
<head>
	<title>Surebet - bookmaker attivi</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<style>
		table tr td{
			padding:2px;
		}
	</style>
</head>

<body>
<h1 style="text-center">Lista bookmaker attivi <span class="small"><a href="index.php"> - Torna all'indice</a></span></h1>

<div class="row">
	<div class="col-sm-3">
		<?php echo $tables[0]; ?>
	</div>
	<div class="col-sm-3">
		<?php echo $tables[1]; ?>
	</div>
	<div class="col-sm-3">
		<?php echo $tables[2]; ?>
	</div>
	<div class="col-sm-3">
		<?php echo $tables[3]; ?>
	</div>
</div>
</body>
</html>
