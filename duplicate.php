<?php
ini_set('display_errors', true);
error_reporting(E_ALL);
include_once "php/Config.php";
include_once "php/Bookmaker.php";
define("NUMBER_HEADER", 20);
$cnf = new Config();

if(isset($_GET['action']) and $_GET['action'] == 'save' and isset($_POST['bookmaker']) and isset($_POST['league']) and isset($_POST['url']) and $_POST['url'] != "" ) {
	$query = "
	SELECT count(bid) as count
	FROM curl where bid = " . $_POST['bookmaker'] . " AND lid=" . $_POST['league'];
	$exec_global = mysqli_query($cnf->getDb(), $query);
	$curl = mysqli_fetch_assoc($exec_global);
	if($curl['count'] == 0){
		// qui va bene
		$query = "INSERT INTO `curl` VALUES(NULL, '".$_POST['bookmaker']."','".$_POST['league']."','".addslashes($_POST['url'])."',1)";
		$exec_global = mysqli_query($cnf->getDb(), $query);
		$cid = mysqli_insert_id($cnf->getDb());
//SELECT * FROM curl_option where cid = (SELECT cid from curl WHERE bid=2 AND cid != 25 limit 1)
		$query = "
		SELECT *
		FROM curl_option where cid = (SELECT cid from curl WHERE bid=" . $_POST['bookmaker'] . " AND cid<>" . $cid . " LIMIT 1) ORDER BY coid ASC";
		$exec_global = mysqli_query($cnf->getDb(), $query);

		while ( $curl_option = mysqli_fetch_assoc($exec_global)) {
			$query_internal = "INSERT INTO `curl_option` VALUES(NULL, '".$cid."','".$curl_option['type']."','".$curl_option['value_']."')";
			$exec_internal = mysqli_query($cnf->getDb(), $query_internal);
		}

		echo '<div class="alert alert-success">Inserimento andato a buon fine.</div>';
	}else{

		echo '<div class="alert alert-danger">Qualcosa è andato storto, riprova ignorante.</div>';
	}
}else{
	if(isset($_GET['action']) and $_GET['action'] == 'save')
	echo '<div class="alert alert-danger">Qualcosa è andato storto, riprova ignorante.</div>';
}
?>

<html>
<head>
	<title>SUREBET duplica cURL</title>
	<script src="js/jquery-2.1.4.min.js"></script>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<meta charset="UTF-8">
</head>

<body>
<h1>Duplica cURL <span class="small"><a href="index.php"> - Torna all'indice</a></span></h1>

<form method="post" action="duplicate.php?action=save">
	<section id="prefix">
		<h2>Informazioni base</h2>
		<div>
			<div class="form-group">
				<label>Bookmaker:</label>
				<select class="form-control" name="bookmaker">
				<?php

				for($i=1;;$i++){
					if(Bookmaker::getBookmaker($i) === false){
						break;
					}
					echo '<option value="'.$i.'">'.Bookmaker::getBookmaker($i).'</option>';
				}
				?>
				</select>
			</div>
		</div>
		<div>
			<div class="form-group">
				<label>Campionato:</label>
				<select class="form-control" name="league">
				<?php
				$query = "
				SELECT L.*
				FROM league AS L ORDER BY L.name ASC";
				$exec_global = mysqli_query($cnf->getDb(), $query);

				while ( $league = mysqli_fetch_assoc($exec_global)) {
					echo '<option value="'.$league['lid'].'">'.$league['name'].'</option>';
				}
				?>
				</select>
			</div>
		</div>
		<div>
			<div class="form-group">
				<label>URL:</label>
				<input type="text" class="form-control" name="url" placeholder="http://porco.dio">
			</div>
		</div>
	</section>
	<button type="submit" class="btn btn-success" style="width: 100%">Duplica</button>
</form>
</body>
</html>