<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
include_once "php/Config.php";
include_once "php/Bookmaker.php";

$return = array();
$cnf = new Config();

?>

<html>
<head>
	<title>Surebet - lista partite</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<style>
		thead{
			background-color: #ccc;
		}
		tr:nth-child(even){
			background-color: #f1f1f1;
		}

		th, td{
			text-align: center;
			padding:10px;
			border: 1px solid #aaaaaa;
		}
	</style>
</head>

<body>
<h1 style="text-center">Lista partite sicure <span class="small"><a href="index.php"> - Torna all'indice</a></span></h1>

<table>
	<thead>
	<th>
		MATCHID
	</th>
	<th>
		PALINSESTO
	</th>
	<th>
		AVVENIMENTO
	</th>
	<th>
		CASA
	</th>
	<th>
		FUORI CASA
	</th>
	<th>
		QUOTA 1
	</th>
	<th>
		QUOTA X
	</th>
	<th>
		QUOTA 2
	</th>
	<th>
		QUANTO VUOI VINCERE?
	</th>
	<th>
		SCOMMETTI SU 1
	</th>
	<th>
		SCOMMETTI SU X
	</th>
	<th>
		SCOMMETTI SU 2
	</th>
	<th>
		RESA
	</th>
	</thead>
	<tbody>

	<?php
		$matches = '';

		$query = "
		SELECT *
		FROM `match`
		";
		$exec_global = mysqli_query($cnf->getDb(), $query);

		$i=0;
		while ( $match = mysqli_fetch_assoc($exec_global)) {
			$yield = round((1-(1/$match['bet_1']+1/$match['bet_x']+1/$match['bet_2']))*100,2) ;
			$matches .= '
				<tr>
					<td>
						'.$match['mid'].'
					</td>
					<td>
						'.$match['pal'].'
					</td>
					<td>
						'.$match['avv'].'
					</td>
					<td>
						'.$match['name_1'].'
					</td>
					<td>
						'.$match['name_2'].'
					</td>
					<td>
						'.number_format($match['bet_1'],2).'
					</td>
					<td>
						'.number_format($match['bet_x'],2).'
					</td>
					<td>
						'.number_format($match['bet_2'],2).'
					</td>
					<td>
						'.$match['toplay'].' &euro;
					</td>
					<td>
						'.Bookmaker::getBookmaker($match['where_bet_1']).'<br>'.number_format(round($match['toplay']/number_format($match['bet_1'],2),0),2).' ('.round($match['toplay']/number_format($match['bet_1'],2),2).') &euro;
					</td>
					<td>
						'.Bookmaker::getBookmaker($match['where_bet_x']).'<br>'.number_format(round($match['toplay']/number_format($match['bet_x'],2),0),2).' ('.round($match['toplay']/number_format($match['bet_x'],2),2).') &euro;
					</td>
					<td>
						'.Bookmaker::getBookmaker($match['where_bet_2']).'<br>'.number_format(round($match['toplay']/number_format($match['bet_2'],2),0),2).' ('.round($match['toplay']/number_format($match['bet_2'],2),2).') &euro;
					</td>
					<td>
						<b>'.$yield.' %</b>
					</td>
				</tr>';
		}

	echo $matches;
	?>
	</tbody>
</table>
</body>
</html>
